# PipeDot

![pipeline](https://gitlab.com/hoop33/pipedot/badges/main/pipeline.svg)
![coverage](https://gitlab.com/hoop33/pipedot/badges/main/coverage.svg?job=test)

> Convert a GitLab pipeline to Graphviz DOT, Mermaid, or Text formats.


![PipeDot Logo](pipedot.png)

This project is under development. Expect frequent and possibly breaking changes.

## Installation

- Install the Rust toolchain: <https://www.rust-lang.org/tools/install>
- Clone this repository: `git clone git@gitlab.com:hoop33/pipedot.git`
- Install: `cd pipedot && cargo install --path .`

## Usage

PipeDot can read from a pipeline file or from `STDIN`. To get help, pass the `--help` parameter:

```console
$ pipedot --help
Convert a GitLab pipeline to Graphviz DOT, Mermaid, or Text formats.

Usage: pipedot [OPTIONS]

Options:
  -p, --pipeline <PIPELINE>  The pipeline file to convert
  -f, --format <FORMAT>      The output format [default: dot] [possible values: dot, mermaid, text]
  -h, --help                 Print help
  -V, --version              Print version
```

Example of passing a pipeline file:

```console
$ pipedot --pipeline .gitlab-ci.yml
strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
  subgraph "cluster_test" {
    label="test"
    "test"[shape=box,fontcolor=darkgreen,color=green]
  }
  subgraph "cluster_build" {
    label="build"
    "build:amd64"[shape=box,fontcolor=darkgreen,color=green]
  }
}
```

Example of using `STDIN`:

```console
$ cat .gitlab-ci.yml | pipedot
strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
  subgraph "cluster_test" {
    label="test"
    "test"[shape=box,fontcolor=darkgreen,color=green]
  }
  subgraph "cluster_build" {
    label="build"
    "build:amd64"[shape=box,fontcolor=darkgreen,color=green]
  }
}
```

Example of text output format:

```console
$ pipedot --pipeline .gitlab-ci.yml --format text
test
  test
build
  build:amd64
```

Example of Mermaid output format:

```console
$ pipedot --pipeline .gitlab-ci.yml --format mermaid
timeline
  title Build Pipeline
  test
    : test
  build
    : build-amd64
```

Example of the Mermaid output, rendered:

```mermaid
timeline
  title Build Pipeline
  test
    : test
  build
    : build-amd64
```

## Configuration

## Roadmap

## Credits

- anyhow
- clap
- graphviz-rust
- yaml-rust2

## Contributing

## License

Copyright &copy; 2024 Rob Warner

Licensed under the [MIT License](https://hoop33.mit-license.org/)


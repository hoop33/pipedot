use crate::pipeline::Pipeline;
use graphviz_rust::dot_generator::{attr, edge, graph, id, node, node_id, subgraph};
use graphviz_rust::dot_structures::*;

pub fn create_dot(pipeline: &Pipeline) -> Graph {
    let mut graph = graph!(strict di id!("pipeline"));
    graph.add_stmt(Stmt::from(attr!("color", "blue")));
    graph.add_stmt(Stmt::from(attr!("fontcolor", "blue")));
    graph.add_stmt(Stmt::from(attr!("rankdir", "TB")));

    pipeline.stages.iter().for_each(|stage| {
        // Don't add the stage if it has no jobs
        if stage.has_visible_jobs() {
            let mut statements: Vec<Stmt> = vec![];
            let label = quote(stage.name.clone());
            statements.push(Stmt::from(attr!("label", label)));

            let jobs = stage.visible_jobs();
            jobs.iter().enumerate().for_each(|(i, job)| {
                // Add the job node
                let id = id!(quote(job.name.clone()));
                let job_node = node!(id; attr!("shape", "box"),
                    attr!("fontcolor", "darkgreen"),
                    attr!("color", "green"));
                statements.push(Stmt::from(job_node));

                // Create invisible edges between each job and the next job so that they are displayed vertically.
                if i < jobs.len() - 1 {
                    let from = node_id!(quote(job.name.clone()));
                    let to = node_id!(quote(jobs[i + 1].name.clone()));
                    let edge = edge!(from => to; attr!("style", "invis"));
                    statements.push(Stmt::from(edge));
                }
            });

            let id = quote(format!("cluster_{}", stage.name.clone()));
            let subgraph = subgraph!(id!(id), statements);
            graph.add_stmt(Stmt::from(subgraph));
        }
    });

    // We set the rank direction from left to right so that the jobs inside the stages are displayed vertically -- they each have the

    graph
}

fn quote(name: String) -> String {
    format!("\"{}\"", name)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::PipeDot;

    #[test]
    fn test_quote() {
        assert_eq!(quote("test".to_string()), "\"test\"");
    }

    #[test]
    fn run_should_create_dot() {
        let yaml = r#"
        stages:
          - test
          - build

        test:
          stage: test
          image: rust:bookworm
          script:
            - apt-get update && apt-get --assume-yes install cloc
            - echo "loc $(cloc src | grep 'SUM' | awk '{print $5}')" > metrics.txt
            - cargo install cargo-llvm-cov
            - cargo llvm-cov
          coverage: '/TOTAL\s*\d*\s*\d*\s*[\d\.]*%\s*\d*\s*\d*\s*[\d\.]*%\s*\d*\s*\d*\s*([\d\.]*)%/'
          artifacts:
            reports:
              metrics: metrics.txt

        build:amd64:
          stage: build
          image: rust
          only:
            - main
          script:
            - cargo build --release
          artifacts:
            paths:
              - target/release/pipedot
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let dot = PipeDot::with_pipeline(pipeline);
        let result = dot.run("dot");
        assert!(result.is_ok());
        assert_eq!(
            result.unwrap(),
            r#"strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
  subgraph "cluster_test" {
    label="test"
    "test"[shape=box,fontcolor=darkgreen,color=green]
  }
  subgraph "cluster_build" {
    label="build"
    "build:amd64"[shape=box,fontcolor=darkgreen,color=green]
  }
}"#
        );
    }

    #[test]
    fn run_should_ignore_stages_without_jobs() {
        let yaml = r#"
        stages:
          - test
          - build

        test:
          stage: test
          script:
            - cargo test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let dot = PipeDot::with_pipeline(pipeline);
        let result = dot.run("dot");
        assert!(result.is_ok());
        assert_eq!(
            result.unwrap(),
            r#"strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
  subgraph "cluster_test" {
    label="test"
    "test"[shape=box,fontcolor=darkgreen,color=green]
  }
}"#
        );
    }

    #[test]
    fn run_should_ignore_stages_with_only_hidden_jobs() {
        let yaml = r#"
        stages:
          - test
          - build

        .test:
          stage: test
          script:
            - cargo test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let dot = PipeDot::with_pipeline(pipeline);
        let result = dot.run("dot");
        assert!(result.is_ok());
        assert_eq!(
            result.unwrap(),
            r#"strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
}"#
        );
    }

    #[test]
    fn run_should_ignore_hidden_jobs() {
        let yaml = r#"
        stages:
          - test
          - build

        test:
          stage: test
          script:
            - cargo test
        .test:
          stage: test
          script:
            - cargo test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let dot = PipeDot::with_pipeline(pipeline);
        let result = dot.run("dot");
        assert!(result.is_ok());
        assert_eq!(
            result.unwrap(),
            r#"strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
  subgraph "cluster_test" {
    label="test"
    "test"[shape=box,fontcolor=darkgreen,color=green]
  }
}"#
        );
    }

    #[test]
    fn run_should_create_invisible_edges_between_jobs_in_the_same_stage() {
        let yaml = r#"
        stages:
          - test

        test:
          stage: test
          script:
            - cargo test
            - cargo build

        test2:
          stage: test
          script:
            - cargo test
            - cargo build

        test3:
          stage: test
          script:
            - cargo test
            - cargo build
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let dot = PipeDot::with_pipeline(pipeline);
        let result = dot.run("dot");
        assert!(result.is_ok());
        assert_eq!(
            result.unwrap(),
            r#"strict digraph pipeline {
  color=blue
  fontcolor=blue
  rankdir=TB
  subgraph "cluster_test" {
    label="test"
    "test"[shape=box,fontcolor=darkgreen,color=green]
    "test" -> "test2" [style=invis]
    "test2"[shape=box,fontcolor=darkgreen,color=green]
    "test2" -> "test3" [style=invis]
    "test3"[shape=box,fontcolor=darkgreen,color=green]
  }
}"#
        );
    }
}

mod dot_format;
pub mod pipeline;
mod text_format;
mod mermaid_format;

use crate::dot_format::create_dot;
use crate::pipeline::Pipeline;
use crate::text_format::create_text;
use anyhow::Result;
use graphviz_rust::printer::{DotPrinter, PrinterContext};
use crate::mermaid_format::create_mermaid;

#[derive(Debug)]
pub struct PipeDot {
    pipeline: Pipeline,
}

impl PipeDot {
    pub fn with_pipeline(pipeline: Pipeline) -> Self {
        Self { pipeline }
    }

    pub fn run(&self, format: &str) -> Result<String> {
        match format {
            "dot" => {
                let dot = create_dot(&self.pipeline);
                Ok(dot
                    .print(&mut PrinterContext::default())
                    .as_str()
                    .to_string())
            }
            "mermaid" => {
                let mermaid = create_mermaid(&self.pipeline);
                Ok(mermaid)
            }
            "text" => {
                let text = create_text(&self.pipeline);
                Ok(text)
            }
            _ => Err(anyhow::anyhow!("Unsupported format: {}", format)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::pipeline::Pipeline;

    #[test]
    fn run_should_return_error_when_format_not_recognized() {
        let yaml = r#"
        stages:
          - test
          - build

        test:
          stage: test
          image: rust:bookworm
          script:
            - cargo test
        build:
          stage: build
          image: rust:bookworm
          script:
            - cargo build
        "#;
        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let pipe_dot = PipeDot::with_pipeline(pipeline);
        let result = pipe_dot.run("invalid");
        assert!(result.is_err());
        assert_eq!(
            result.unwrap_err().to_string(),
            "Unsupported format: invalid"
        );
    }
}

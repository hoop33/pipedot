use anyhow::Result;
use clap::builder::PossibleValuesParser;
use clap::Parser;
use pipedot::pipeline::Pipeline;
use pipedot::PipeDot;
use std::fs::File;
use std::io::{stdin, Read};
use std::path::PathBuf;

/// Convert a GitLab pipeline to Graphviz DOT, Mermaid, or Text formats.
#[derive(Debug, Parser)]
#[clap(version, about, long_about = None)]
struct Args {
    /// The pipeline file to convert
    #[clap(short, long)]
    pipeline: Option<PathBuf>,
    /// The output format
    #[clap(short, long, default_value = "dot", value_parser = PossibleValuesParser::new(&["dot", "mermaid", "text"]))]
    format: String,
}

fn read_data(path: &Option<PathBuf>) -> Result<String> {
    match path {
        Some(path) => {
            let mut file = File::open(path)?;
            let mut data = String::new();
            file.read_to_string(&mut data)?;
            Ok(data)
        }
        None => {
            let mut data = String::new();
            stdin().read_to_string(&mut data)?;
            Ok(data)
        }
    }
}

fn main() {
    let args = Args::parse();
    let data = match read_data(&args.pipeline) {
        Ok(data) => data,
        Err(e) => {
            eprintln!("Error: {}", e);
            return;
        }
    };
    let pipeline = match Pipeline::from_yaml(&data) {
        Ok(pipeline) => pipeline,
        Err(e) => {
            eprintln!("Error: {}", e);
            return;
        }
    };
    let pipe_dot = PipeDot::with_pipeline(pipeline);
    match pipe_dot.run(&args.format) {
        Ok(dot) => println!("{}", dot),
        Err(e) => eprintln!("Error: {}", e),
    }
}

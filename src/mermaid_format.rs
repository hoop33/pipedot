use crate::pipeline::Pipeline;

pub fn create_mermaid(pipeline: &Pipeline) -> String {
    let mut mermaid = String::new();
    mermaid.push_str("timeline\n");
    mermaid.push_str("  title Build Pipeline\n");

    pipeline.stages.iter().for_each(|stage| {
        if stage.has_visible_jobs() {
            mermaid.push_str(&format!("  {}\n", clean(stage.name.clone())));
            stage.jobs.iter().for_each(|job| {
                if !job.is_hidden() {
                    mermaid.push_str(&format!("    : {}\n", clean(job.name.clone())));
                }
            });
        }
    });

    mermaid
}

fn clean(input: String) -> String {
    // TODO wrap text -- maybe insert a space every n characters?
    input.replace(":", "-")
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::PipeDot;

    #[test]
    fn clean_should_replace_colon_with_dash() {
        assert_eq!(clean("test:job".to_string()), "test-job");
    }

    #[test]
    fn run_should_create_mermaid() {
        let yaml = r#"
        stages:
          - test
          - build

        test:
          stage: test
          image: rust:bookworm
          script:
            - cargo test
        test-2:
          stage: test
          image: rust:bookworm
          script:
            - cargo test
        build:
          stage: build
          image: rust:bookworm
          script:
            - cargo build
        "#;
        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let pipe_dot = PipeDot::with_pipeline(pipeline);
        let text = pipe_dot.run("mermaid").unwrap();
        assert_eq!(text, "timeline\n  title Build Pipeline\n  test\n    : test\n    : test-2\n  build\n    : build\n");
    }
}

use anyhow::{anyhow, Result};
use std::collections::BTreeMap;
use yaml_rust2::{yaml, Yaml};

const DEFAULT_STAGES: [&str; 5] = [".pre", "build", "test", "deploy", ".post"];
const HIDDEN_MARKER: &str = ".";
const STAGE_WHEN_NOT_SPECIFIED: &str = "test";

#[derive(Debug)]
pub struct Pipeline {
    pub stages: Vec<Stage>,
}

#[derive(Debug)]
pub struct Stage {
    pub name: String,
    pub jobs: Vec<Job>,
}

#[derive(Clone, Debug)]
pub struct Job {
    pub name: String,
    pub stage_name: String,
    pub extends: Vec<String>,
}

impl Pipeline {
    pub fn from_yaml(yaml: &str) -> Result<Pipeline> {
        let result = yaml::YamlLoader::load_from_str(yaml)?;
        match result.len() {
            1 => parse_pipeline(&result[0]),
            0 => Err(anyhow!("No YAML documents found in input")),
            _ => Err(anyhow!("Multiple YAML documents found in input")),
        }
    }

    fn with_stages(stage_names: Vec<String>) -> Pipeline {
        Pipeline {
            stages: stage_names
                .iter()
                .map(|name| Stage {
                    name: name.to_string(),
                    jobs: Vec::new(),
                })
                .collect(),
        }
    }

    fn add_job(&mut self, job: Job) {
        for stage in &mut self.stages {
            if stage.name == job.stage_name {
                stage.jobs.push(job);
                return;
            }
        }
    }
}

fn parse_pipeline(yaml: &Yaml) -> Result<Pipeline> {
    let mut pipeline = Pipeline::with_stages(get_stage_names(yaml));

    let jobs = parse_jobs(yaml)?;
    let resolved_jobs = resolve_extends(jobs);
    for mut job in resolved_jobs {
        if job.stage_name.is_empty() {
            job.stage_name = STAGE_WHEN_NOT_SPECIFIED.to_string();
        }
        pipeline.add_job(job);
    }

    Ok(pipeline)
}

fn parse_jobs(yaml: &Yaml) -> Result<BTreeMap<String, Job>> {
    let mut jobs = BTreeMap::new();
    if let Yaml::Hash(ref h) = *yaml {
        for (k, v) in h {
            k.as_str().and_then(|name| {
                if !is_keyword(name) {
                    parse_job(yaml, name, v).map(|job| jobs.insert(name.to_string(), job))
                } else {
                    None
                }
            });
        }
    }
    Ok(jobs)
}

fn parse_job(_yaml: &Yaml, name: &str, job_yaml: &Yaml) -> Option<Job> {
    match job_yaml {
        Yaml::Hash(h) => {
            let mut job = Job {
                name: name.to_string(),
                stage_name: "".to_string(),
                extends: Vec::new(),
            };
            for (k, v) in h {
                k.as_str().and_then(|s| match s {
                    "stage" => v.as_str().map(|s| job.stage_name = s.to_string()),
                    "extends" => v.as_vec().map(|a| {
                        for item in a {
                            if let Some(s) = item.as_str() {
                                job.extends.push(s.to_string())
                            }
                        }
                    }),
                    _ => None,
                });
            }
            Some(job)
        }
        _ => None,
    }
}

fn resolve_extends(jobs: BTreeMap<String, Job>) -> Vec<Job> {
    // Rules:
    // 1. If a job has a stage name already, use it.
    // 2. If a job extends another job, use the stage name of the job being extended.
    // 3. If a job extends multiple jobs, use the stage name of the first job being extended.
    jobs.values()
        .map(|job| {
            let mut resolved_job = job.clone();
            if resolved_job.stage_name.is_empty() {
                resolved_job.stage_name = resolved_job
                    .extends
                    .iter()
                    .filter_map(|extend| jobs.get(extend))
                    .find(|extended_job| !extended_job.stage_name.is_empty())
                    .map_or(String::new(), |extended_job| {
                        extended_job.stage_name.clone()
                    });
            }
            resolved_job
        })
        .collect()
}

fn get_stage_names(yaml: &Yaml) -> Vec<String> {
    let stage_names = &yaml["stages"].as_vec();
    if stage_names.is_none() || stage_names.unwrap().is_empty() {
        return DEFAULT_STAGES.iter().map(|s| s.to_string()).collect();
    }
    stage_names
        .unwrap()
        .iter()
        .map(|s| s.as_str().unwrap().to_string())
        .collect()
}

fn is_keyword(name: &str) -> bool {
    ["default", "include", "stages", "variables", "workflow"].contains(&name)
}

impl Stage {
    pub fn has_visible_jobs(&self) -> bool {
        self.jobs.iter().any(|job| !job.is_hidden())
    }

    pub fn visible_jobs(&self) -> Vec<&Job> {
        self.jobs.iter().filter(|job| !job.is_hidden()).collect()
    }
}

impl Job {
    pub fn is_hidden(&self) -> bool {
        self.name.starts_with(HIDDEN_MARKER)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn pipeline_should_have_default_stages_when_not_specified() {
        let yaml = r#"
        test:
            stage: build
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 5);
        assert_eq!(pipeline.stages[0].name, ".pre");
        assert_eq!(pipeline.stages[1].name, "build");
        assert_eq!(pipeline.stages[2].name, "test");
        assert_eq!(pipeline.stages[3].name, "deploy");
        assert_eq!(pipeline.stages[4].name, ".post");
    }

    #[test]
    fn pipeline_should_have_specified_stages() {
        let yaml = r#"
        stages:
            - build
            - test
            - deploy
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 3);
        assert_eq!(pipeline.stages[0].name, "build");
        assert_eq!(pipeline.stages[1].name, "test");
        assert_eq!(pipeline.stages[2].name, "deploy");
    }

    #[test]
    fn pipeline_should_add_jobs_to_stages() {
        let yaml = r#"
        stages:
            - build
            - test
            - deploy
        test:
            stage: test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 3);
        assert_eq!(pipeline.stages[0].name, "build");
        assert_eq!(pipeline.stages[1].name, "test");
        assert_eq!(pipeline.stages[2].name, "deploy");
        assert_eq!(pipeline.stages[1].jobs.len(), 1);
        assert_eq!(pipeline.stages[1].jobs[0].name, "test");
        assert!(pipeline.stages[0].jobs.is_empty());
        assert!(pipeline.stages[2].jobs.is_empty());
    }

    #[test]
    fn from_yaml_should_return_error_when_invalid_yaml() {
        let yaml = r#"
        stages:
            - build
        stages:
            - build
        "#;

        let result = Pipeline::from_yaml(yaml);
        assert!(result.is_err());
        assert_eq!(
            result.err().unwrap().to_string(),
            "String(\"stages\"): duplicated key in mapping at byte 81 line 6 column 9"
        );
    }

    #[test]
    fn from_yaml_should_return_error_when_no_documents() {
        let yaml = "";

        let result = Pipeline::from_yaml(yaml);
        assert!(result.is_err());
        assert_eq!(
            result.err().unwrap().to_string(),
            "No YAML documents found in input"
        );
    }

    #[test]
    fn from_yaml_should_not_create_jobs_from_keywords() {
        let yaml = r#"
        stages:
            - build
        default:
            stage: build
        include:
            stage: build
        variables:
            stage: build
        workflow:
            stage: build
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].name, "build");
        assert!(pipeline.stages[0].jobs.is_empty());
    }

    #[test]
    fn job_should_have_test_stage_when_not_specified() {
        let yaml = r#"
        stages:
            - test
        test:
            script:
                - echo Hello
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].jobs.len(), 1);
        assert_eq!(pipeline.stages[0].jobs[0].stage_name, "test");
    }

    #[test]
    fn job_is_hidden_should_return_true_when_name_starts_with_dot() {
        let yaml = r#"
        stages:
            - test
        .test:
            stage: test
            .script:
                - echo Hello
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].jobs.len(), 1);
        assert!(pipeline.stages[0].jobs[0].is_hidden());
    }

    #[test]
    fn job_is_hidden_should_return_false_when_name_does_not_start_with_dot() {
        let yaml = r#"
        stages:
            - test
        test:
            stage: test
            script:
                - echo Hello
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].jobs.len(), 1);
        assert!(!pipeline.stages[0].jobs[0].is_hidden());
    }

    #[test]
    fn stage_has_visible_jobs_should_return_true_when_jobs_are_not_hidden() {
        let yaml = r#"
        stages:
            - test
        test:
            stage: test
            script:
                - echo Hello
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert!(pipeline.stages[0].has_visible_jobs());
    }

    #[test]
    fn stage_has_visible_jobs_should_return_false_when_all_jobs_are_hidden() {
        let yaml = r#"
        stages:
            - test
        .test:
            stage: test
            .script:
                - echo Hello
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert!(!pipeline.stages[0].has_visible_jobs());
    }

    #[test]
    fn stage_has_visible_jobs_should_return_false_when_no_jobs_are_specified() {
        let yaml = r#"
        stages:
            - test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert!(!pipeline.stages[0].has_visible_jobs());
    }

    #[test]
    fn stage_has_visible_jobs_should_return_true_when_one_job_is_visible() {
        let yaml = r#"
        stages:
            - test
        test:
            stage: test
        .hidden:
            stage: test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert!(pipeline.stages[0].has_visible_jobs());
    }

    #[test]
    fn job_should_extend_other_jobs() {
        let yaml = r#"
        stages:
            - test
        test:
            stage: test
        test2:
            stage: test
            extends:
                - test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].jobs.len(), 2);
        assert_eq!(pipeline.stages[0].jobs[0].name, "test");
        assert_eq!(pipeline.stages[0].jobs[1].name, "test2");
        assert_eq!(pipeline.stages[0].jobs[1].extends.len(), 1);
        assert_eq!(pipeline.stages[0].jobs[1].extends[0], "test");
    }

    #[test]
    fn job_should_use_own_stage_name_when_specified() {
        let yaml = r#"
        stages:
            - test
            - build
        test:
            stage: test
        build:
            stage: build
            extends:
                - test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 2);
        assert_eq!(pipeline.stages[1].jobs.len(), 1);
        assert_eq!(pipeline.stages[1].jobs[0].name, "build");
        assert_eq!(pipeline.stages[1].jobs[0].stage_name, "build");
    }

    #[test]
    fn job_should_use_extends_stage_name_when_not_specified() {
        let yaml = r#"
        stages:
            - test
        test:
            stage: test
        test2:
            extends:
                - test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].jobs.len(), 2);
        assert_eq!(pipeline.stages[0].jobs[0].name, "test");
        assert_eq!(pipeline.stages[0].jobs[1].name, "test2");
        assert_eq!(pipeline.stages[0].jobs[1].stage_name, "test");
    }

    #[test]
    fn job_should_use_test_stage_when_no_stage_specified() {
        let yaml = r#"
        stages:
            - test
        test:
            script:
                - echo Hello
        test2:
            script:
                - echo Hello
            extends:
                - test
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 1);
        assert_eq!(pipeline.stages[0].jobs.len(), 2);
        assert_eq!(pipeline.stages[0].jobs[0].name, "test");
        assert_eq!(pipeline.stages[0].jobs[1].name, "test2");
        assert_eq!(pipeline.stages[0].jobs[1].stage_name, "test");
    }

    #[test]
    fn job_should_use_first_extends_stage_name_when_multiple_extends() {
        let yaml = r#"
        stages:
            - test
            - build
            - .post
        test:
            script:
                - echo Hello
        build:
            stage: build
        build2:
            extends:
                - test
                - build
                - .post
        .post:
            stage: .post
        "#;

        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        assert_eq!(pipeline.stages.len(), 3);
        assert_eq!(pipeline.stages[1].jobs.len(), 2);
        assert_eq!(pipeline.stages[1].jobs[0].name, "build");
        assert_eq!(pipeline.stages[1].jobs[1].name, "build2");
        assert_eq!(pipeline.stages[1].jobs[1].stage_name, "build");
    }
}

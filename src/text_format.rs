use crate::pipeline::Pipeline;

pub fn create_text(pipeline: &Pipeline) -> String {
    let mut text = String::new();

    pipeline.stages.iter().for_each(|stage| {
        if stage.has_visible_jobs() {
            text.push_str(&format!("{}\n", stage.name));
            stage.jobs.iter().for_each(|job| {
                if !job.is_hidden() {
                    text.push_str(&format!("  {}\n", job.name));
                }
            });
        }
    });

    text
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::PipeDot;

    #[test]
    fn run_should_create_text() {
        let yaml = r#"
        stages:
          - test
          - build

        test:
          stage: test
          image: rust:bookworm
          script:
            - cargo test
        build:
          stage: build
          image: rust:bookworm
          script:
            - cargo build
        "#;
        let pipeline = Pipeline::from_yaml(yaml).unwrap();
        let pipe_dot = PipeDot::with_pipeline(pipeline);
        let text = pipe_dot.run("text").unwrap();
        assert_eq!(text, "test\n  test\nbuild\n  build\n");
    }
}
